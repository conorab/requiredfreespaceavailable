#!/bin/bash

# requiredFreeSpaceAvailable - v1.0 - Check if given amount of free space is available for given location.

# Arguments/Environment Variables.

	# target - The path (doesn't need to be a mount point) whose free space we will check.
	# requiredFree - The amount of free space in kilobytes which needs to be available.

# Exit Codes

	# 0 - Required free space is available.
	# 1 - Required free space is not available.
	# 2 - The 'requiredFree' environment variable is null.
	# 3 - Failed to get the block device and available space for "$target".
	# 4 - Failed to get the block size of the device used by "$target".
	# 5 - The 'target' environment variable is null.
	# 6 - The 'requiredFree' variable is not an integer.
	# 7 - The 'requiredFree' variable is less than zero.

# Make sure our input variables have been provided.

	if [ -z "$target" ]

		then
			echo "$0"': The '\''target'\'' environment variable is null.' >&2
			exit 5
	fi

	if [ -z "$requiredFree" ]

		then
			echo "$0"': The '\''requiredFree'\'' environment variable is null.' >&2
			exit 2
	fi

# Make sure the 'requiredFree' variable is a number and greater than zero.

	if ! [ "$requiredFree" -eq "$requiredFree" ] &> /dev/null

		then
			echo "$0"': The '\''requiredFree'\'' variable is not an integer.' >&2
			exit 6
	fi

	if [ "$requiredFree" -lt 0 ]

		then
			echo "$0"': The '\''requiredFree'\'' environment variable is less than 0.' >&2
			exit 7
	fi

#export target=/; export requiredFree=$(($((1024^2))*200))

# Get the block device and free space for "$target".
# We need the exit code of 'df' and 'cut', so we're using PIPESTATUS to get the exit code of both commands in the pipe and adding them.

	IFS=' '
	if ! DFOut=($(df --block-size=K --output=avail,source "$target" 2> /dev/null | cut -d $'\n' -f2- 2> /dev/null; exit $((${PIPESTATUS[0]}+${PIPESTATUS[1]}))))

		then
			echo "$0"': Failed to get the block device and available space for '\'"$target"\'\. >&2
			exit 3
	fi

# Find the block size for the block device used by "$target".

	unset targetBlockSize
	if ! targetBlockSize="$(blockdev --getbsz ${DFOut[1]} 2> /dev/null)"

		then
			echo "$0"': Failed to get the block size of '\'"${DFOut[1]}"\'' (target: '\'"$target"\'').' >&2
			exit 4
	fi

# Check if the amount of free space in the mount point which "$target" is under is greater than that specified in "$requiredFree".
# All sizes are in kilobytes.

	unset DFOut_availableNoK
	DFOut_availableNoK="${DFOut[0]}"

	if [ "${DFOut_availableNoK:0:$((${#DFOut_availableNoK}-1))}" -gt "$requiredFree" ]

		then
			exit 0
		else
			exit 1
	fi


